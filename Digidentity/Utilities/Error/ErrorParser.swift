//
//  ErrorParser.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 04/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

class ErrorParser {
    
    // MARK: - Variables
    private(set) var domain: String
    private(set) var code: Int
    
    
    // MARK: - Initializer
    init(domain: String = "Digidentity", code: Int = 0) {
        self.domain = domain
        self.code = code
    }
    
    
    // MARK: - Control
    func getErrorWithLocalizedDescription(_ localizedDescription: String) -> Error {
        return NSError.init(domain: domain, code: code, userInfo: [NSLocalizedDescriptionKey: localizedDescription])
    }
}
