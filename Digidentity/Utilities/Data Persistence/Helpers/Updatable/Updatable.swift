//
//  Updatable.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 04/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import RealmSwift
import Unbox

protocol Updatable: class { }


extension Updatable where Self: Object {
    static func updateObject(_ dictionary: [String:Any], realm: Realm) throws -> Self {
        let unboxer = Unboxer(dictionary: dictionary)
        let unboxableObject = try self.unboxable(unboxer: unboxer)
        
        guard let realmObject = unboxableObject as? Object else {
            fatalError()
        }
        
        try realm.write {
            realm.add(realmObject, update: true)
        }
        
        guard let realmObjectSelf = realmObject as? Self else {
            fatalError()
        }
        
        return realmObjectSelf
    }
    
    
    static func updateObjects(_ dictionaryArray: [[String:Any]], realm: Realm) throws -> [Self] {
        var objects: [Self] = []
        
        for dictionary in dictionaryArray {
            let object = try self.updateObject(dictionary, realm: realm)
            objects.append(object)
        }
        
        return objects
    }
}
