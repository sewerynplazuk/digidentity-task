//
//  CatalogItem.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 04/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import RealmSwift
import Unbox

class CatalogItem: Object, Unboxable, Updatable {
    
    // MARK: - Properties
    @objc dynamic var id: String = ""
    @objc dynamic var text: String = ""
    @objc dynamic var confidence: Float = 0.0
    @objc dynamic var encodedImage: String = ""
    
    
    // MARK: - Primary Key
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    // MARK: - Unboxable
    required convenience init(unboxer: Unboxer) throws {
        self.init()
        self.id = try unboxer.unbox(key: "_id")
        self.text = try unboxer.unbox(key: "text")
        self.confidence = try unboxer.unbox(key: "confidence")
        self.encodedImage = try unboxer.unbox(key: "img")
    }
}
