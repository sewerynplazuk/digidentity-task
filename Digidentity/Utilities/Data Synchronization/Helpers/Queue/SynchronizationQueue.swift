//
//  SynchronizationQueue.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 04/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation


class SynchronizationQueue {
    
    // MARK: - Variables
    private(set) static var background = DispatchQueue(label: "com.digidentity.data.synchronization.background", qos: .background, attributes: .concurrent, target: nil)
    
    
    // MARK: - Initailizer
    private init() { }
}
