//
//  Synchronizable.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 04/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import RealmSwift

protocol Synchronizable: class { }

extension Synchronizable {
    func deleteLocalDifferences(_ realm: Realm, fetchedObjects: [Object], persistedObjects: [Object]) {
        for object in persistedObjects {
            if object.isInvalidated && fetchedObjects.contains(object) {
                object.delete(realm: realm)
            } else {
                continue
            }
        }
    }
}
