//
//  CatalogItemSync.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 04/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import PromiseKit
import RealmSwift

class CatalogItemSync: Synchronizable {
    
    // MARK: - Variables
    private var request: Request!
    
    
    // MARK: - Control
    func synchornize(sinceId: String?, maxId: String?) -> Promise<Void> {
        self.request = Request(route: .catalogItems(sinceId: sinceId, maxId: maxId))
        return request.getAll().then(on: SynchronizationQueue.background, flags: nil) { itemsDictArray -> Promise<Void> in
            let realm = try Realm()
            
            let fetchedItems = try CatalogItem.updateObjects(itemsDictArray, realm: realm)
            let persistedItems: [CatalogItem] = CatalogItem.getPersistedObjects(realm: realm)
            
            self.deleteLocalDifferences(realm, fetchedObjects: fetchedItems, persistedObjects: persistedItems)
            
            return Promise()
        }
    }
}
