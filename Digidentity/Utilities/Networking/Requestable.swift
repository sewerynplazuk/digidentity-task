//
//  Requestable.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 05/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

protocol Requestable: class { }

extension Requestable {
    func configureGetRequest(_ request: inout URLRequest) {
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("a6f501134e33113888e81dc3043ebbfb", forHTTPHeaderField: "Authorization")
    }
    
    
    func validateStatusCode(_ statusCode: Int) -> Bool {
        return 200...399 ~= statusCode
    }
}
