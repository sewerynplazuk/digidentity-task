//
//  Parameter.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 05/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

class Parameter {
    
    // MARK: - Variables
    var name: String
    var value: String
    
    
    // MARK: - Initializer
    init(name: String, value: String) {
        self.name = name
        self.value = value
    }
}
