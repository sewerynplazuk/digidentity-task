//
//  URLSessionCertificatePinningDelegate.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 05/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

class URLSessionCertificatePinningDelegate: NSObject {

    // MARK: - Helpers
    private func getLocalCertificateData() -> Data? {
        guard
            let localCertificatePath = Bundle.main.path(forResource: "marlove", ofType: "cer"),
            let localCertificateData = try? NSData(contentsOfFile: localCertificatePath) as Data
        else { return nil }
    
        return localCertificateData
    }
}


// MARK: - URLSessionDelegate
extension URLSessionCertificatePinningDelegate: URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard
            challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust,
            let serverTrust = challenge.protectionSpace.serverTrust,
            let localCertificateData = self.getLocalCertificateData()
        else {
            completionHandler(.cancelAuthenticationChallenge, nil)
            return
        }
        
        var secResult: SecTrustResultType = .invalid
        let status = SecTrustEvaluate(serverTrust, &secResult)
        let isServerTrusted: Bool = secResult == .proceed || secResult == .unspecified
        
        guard status == errSecSuccess && isServerTrusted, let remoteCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) else {
            completionHandler(.cancelAuthenticationChallenge, nil)
            return
        }
        
        
        let remoteCertificateData: Data = SecCertificateCopyData(remoteCertificate) as Data

        if remoteCertificateData == localCertificateData {
            let credential = URLCredential(trust: serverTrust)
            completionHandler(.useCredential, credential)
        } else {
            completionHandler(.cancelAuthenticationChallenge, nil)
        }
    }
}
