//
//  DataDecoder.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 05/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

class DataDecoder {
    
    // MARK: - Variables
    private(set) var data: Data
    private lazy var errorParser = ErrorParser()
    
    // MARK: - Initializer
    init(data: Data) {
        self.data = data
    }
    
    
    // MARK: - Decoding
    func decodeDataToJsonArray() throws -> [[String:Any]] {
        let anyJson = try JSONSerialization.jsonObject(with: data, options: [])
        
        guard let jsonArray = anyJson as? [[String:Any]] else {
            throw errorParser.getErrorWithLocalizedDescription("Request returned data which cannot be parsed to [[String:Any]] type.")
        }
        
        return jsonArray
    }
}
