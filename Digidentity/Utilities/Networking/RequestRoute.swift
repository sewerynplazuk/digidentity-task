//
//  RequestRoute.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 05/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

enum RequestRoute {
    case catalogItems(sinceId: String?, maxId: String?)
    
    // MARK: - Variables
    var url: URL {
        switch self {
        case .catalogItems(let sinceId, let maxId):
            guard let url = URL(string: "https://marlove.net/e/mock/v1/items") else {
                fatalError("URL mustn't be nil")
            }
            
            var parameters: [Parameter] = []
            if let unwrappedSinceId = sinceId {
                parameters.append(Parameter.init(name: "since_id", value: unwrappedSinceId))
            }
            if let unwrappedMaxId = maxId {
                parameters.append(Parameter.init(name: "max_id", value: unwrappedMaxId))
            }
            
            let parametersEncoder = ParametersEncoder(baseUrlString: url.absoluteString, parameters: parameters)
            if let encodedUrl = parametersEncoder.encodeUrl() {
                return encodedUrl
            } else {
                return url
            }
        }
    }
}
