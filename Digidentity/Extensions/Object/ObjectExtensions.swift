//
//  ObjectExtensions.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 04/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import RealmSwift
import Unbox

extension Object {
    static func unboxable(unboxer: Unboxer) throws -> Unboxable {
        guard let unboxable = self as? Unboxable.Type else {
            throw ErrorParser().getErrorWithLocalizedDescription("Unable to cast Object to Unboxable.Type")
        }
        return try unboxable.init(unboxer: unboxer)
    }
    
    
    static func getPersistedObjects<T: Object>(realm: Realm) -> [T] {
        return Array(realm.objects(T.self))
    }
    
    
    func delete(realm: Realm) {
        try? realm.write {
            realm.delete(self)
        }
    }
}
