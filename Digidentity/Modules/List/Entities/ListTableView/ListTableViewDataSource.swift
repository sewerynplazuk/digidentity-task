//
//  ListTableViewDataSource.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 06/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

class ListTableViewDataSource: NSObject, UITableViewDataSource {
    
    // MARK: - Variables
    private(set) var catalogItems: [CatalogItem] = []
    
    
    // MARK: - Lifecycle
    func populateCatalogItems(_ catalogItems: [CatalogItem]) {
        self.catalogItems = catalogItems
    }
    
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return catalogItems.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(UINib(nibName: "CatalogItemTableViewCell", bundle: nil), forCellReuseIdentifier: CatalogItemTableViewCell.cellIdentifier)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CatalogItemTableViewCell.cellIdentifier, for: indexPath) as? CatalogItemTableViewCell else {
            fatalError()
        }
        
        let catalogItem = self.catalogItems[indexPath.row]
        
        let cellDataSource = CatalogItemTableViewCellDataSource(cell: cell, catalogItem: catalogItem)
        cell.dataSource = cellDataSource
        
        return cell
    }
}
