//
//  CatalogItemsObserver.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 06/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import RealmSwift

class CatalogItemsObserver {
    
    // MARK: - Variables
    private(set) var catalogItems: Results<CatalogItem>?
    private var realm: Realm?
    private var realmNotificationToken: NotificationToken?
    
    
    // MARK: - Initializer
    init(realm: Realm? = try? Realm()) {
        self.realm = realm
        if realm == nil {
            assertionFailure() // Shouldn't happen when no migration is required
        }
        
        self.catalogItems = realm?.objects(CatalogItem.self)
    }
    
    
    deinit {
        realmNotificationToken?.invalidate()
    }
    
    
    // MARK: - Register
    @discardableResult
    func registerForDataUpdateNotifications(_ completion: @escaping (_ error: Error?) -> Void) -> Bool {
        guard let catalogItems = self.catalogItems else {
            return false
        }
        
        realmNotificationToken = catalogItems.observe { change in
            switch change {
            case .error(let error): completion(error)
            default: completion(nil)
            }
        }
        
        return true
    }
}
