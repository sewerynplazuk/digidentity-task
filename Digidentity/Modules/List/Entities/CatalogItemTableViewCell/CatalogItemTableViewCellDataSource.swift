//
//  CatalogItemTableViewCellDataSource.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 06/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

class CatalogItemTableViewCellDataSource {
    
    // MARK: - Variables
    private weak var cell: CatalogItemTableViewCell?
    private var catalogItem: CatalogItem
    
    
    // MARK: - Initializers
    init(cell: CatalogItemTableViewCell, catalogItem: CatalogItem) {
        self.cell = cell
        self.catalogItem = catalogItem
        self.populateCell()
    }
    
    
    // MARK: - Setup subviews
    private func populateCell() {
        self.cell?.itemIdLabel.text = "ID: \(catalogItem.id)"
        self.cell?.itemConfidenceLabel.text = "Confidence: \(catalogItem.confidence)"
        self.cell?.itemTextLabel.text = catalogItem.text
        self.cell?.itemImageView.image = catalogItemImage
    }
    
    
    // MARK: - Helpers
    private var catalogItemImage: UIImage? {
        guard let imageData = Data(base64Encoded: catalogItem.encodedImage) else { return nil }
        return UIImage(data: imageData)
    }
}
