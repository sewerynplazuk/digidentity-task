//
//  ListPresenter.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 06/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

class ListPresenter: ListModuleInterface {
    
    // MARK: - Variables
    weak var view: ListViewInterface!
    var interactor: ListInteractorInterface!
    
    
    // MARK: - ListModuleInterface
    func onViewDidLoad() {
        self.interactor.clearRedundantCache()
        
        self.view.setTableViewDataSource(interactor.tableViewDataSource)
        self.view.setTableViewDelegate(interactor.tableViewDelegate)
        self.view.addRefreshControlToTableView()
        
        self.registerForDataUpdateNotification()
        self.registerForWillDisplayLastCellCallback()
        
        self.interactor.fetchItems().done {
            print("Yaaay! Successfully fetched items on viewDidLoad.")
        }.catch { error in
            print("Fetching items on viewDidLoad failed with error \(error.localizedDescription)")
        }
    }
    
    
    func onRefreshAction() {
        self.interactor.fetchNewerItems().done {
            print("Yaaay! Successfully fetched newer items.")
        }.catch { error in
            print("Fetching newer items failed with error \(error.localizedDescription)")
        }.finally { [weak self] in
            self?.view.endRefreshingOnRefreshControl()
        }
    }
    
    
    // MARK: - Helpers
    private func registerForDataUpdateNotification() {
        self.interactor.registerForDataUpdateNotification { [weak self] shouldUpdateTableView in
            guard shouldUpdateTableView else { return }
            self?.view.reloadDataInTableView()
        }
    }
    
    
    private func registerForWillDisplayLastCellCallback() {
        self.interactor.registerForWillDisplayLastCellCallback() { [weak self] in
            self?.interactor.fetchOlderItems().done {
                print("Yaaay! Successfully fetched older items.")
            }.catch { error in
                print("Fetching older items failed with error \(error.localizedDescription)")
            }
        }
    }
}
