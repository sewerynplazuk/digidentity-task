
//
//  ListModuleInterface.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 06/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation

protocol ListModuleInterface: class {
    func onViewDidLoad()
    func onRefreshAction()
}
