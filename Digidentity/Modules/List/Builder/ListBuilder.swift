//
//  ListBuilder.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 06/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

class ListBuilder {
    
    // MARK: - Class lifecycle
    private init() {}
    
    
    // MARK: - Control
    static func build() -> ListViewController {
        guard let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListViewController") as? ListViewController else {
            fatalError()
        }
        let interactor = ListInteractor()
        let presenter = ListPresenter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        
        return view
    }

}
