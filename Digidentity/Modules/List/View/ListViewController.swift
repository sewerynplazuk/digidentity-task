//
//  ListViewController.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 06/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, ListViewInterface {
    
    // MARK: - Variables
    var presenter: ListModuleInterface!
    private var refreshControl: UIRefreshControl?
    
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.onViewDidLoad()
    }
    
    
    // MARK: - ListViewInterface
    func setTableViewDataSource(_ dataSource: UITableViewDataSource) {
        self.tableView.dataSource = dataSource
    }
    
    
    func setTableViewDelegate(_ delegate: UITableViewDelegate) {
        self.tableView.delegate = delegate
    }
    
    
    func reloadDataInTableView() {
        self.tableView.reloadData()
    }
    
    
    func addRefreshControlToTableView() {
        self.refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(self.handleRefreshAction), for: .valueChanged)
        
        self.tableView.refreshControl = refreshControl
    }
    
    
    func endRefreshingOnRefreshControl() {
        self.refreshControl?.endRefreshing()
    }
    
    
    // MARK: - UIRefreshControl
    @objc private func handleRefreshAction() {
        self.presenter.onRefreshAction()
    }
}
