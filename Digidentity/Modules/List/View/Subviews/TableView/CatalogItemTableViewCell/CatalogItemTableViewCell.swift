//
//  CatalogItemTableViewCell.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 06/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

class CatalogItemTableViewCell: UITableViewCell {
    
    // MARK: - Constants
    static let cellIdentifier: String = "CatalogItemTableViewCell"
    
    
    // MARK: - Variables
    var dataSource: CatalogItemTableViewCellDataSource?
    
    
    // MARK: - IBOutlets
    @IBOutlet weak var itemIdLabel: UILabel!
    @IBOutlet weak var itemConfidenceLabel: UILabel!
    @IBOutlet weak var itemTextLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
}
