//
//  ListViewInterface.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 06/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import UIKit

protocol ListViewInterface: class {
    func setTableViewDataSource(_ dataSource: UITableViewDataSource)
    func setTableViewDelegate(_ delegate: UITableViewDelegate)
    func reloadDataInTableView()
    func addRefreshControlToTableView()
    func endRefreshingOnRefreshControl()
}
