//
//  ListInteractorInterface.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 06/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import PromiseKit

protocol ListInteractorInterface: class {
    var tableViewDataSource: ListTableViewDataSource { get }
    var tableViewDelegate: ListTableViewDelegate { get }
    func registerForDataUpdateNotification(_ completion: @escaping (_ shouldUpdateTableView: Bool) -> Void)
    func registerForWillDisplayLastCellCallback(_ callback: @escaping () -> Void)
    func fetchItems() -> Promise<Void>
    func fetchNewerItems() -> Promise<Void>
    func fetchOlderItems() -> Promise<Void>
    func clearRedundantCache()
}
