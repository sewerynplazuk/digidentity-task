//
//  ListInteractor.swift
//  Digidentity
//
//  Created by Seweryn Plażuk on 06/11/2018.
//  Copyright © 2018 Seweryn Plażuk. All rights reserved.
//

import Foundation
import RealmSwift
import PromiseKit

class ListInteractor: ListInteractorInterface {
    
    // MARK: - Variables
    private var catalogItemsObserver: CatalogItemsObserver
    private var catalogItemsSync: CatalogItemSync
    
    
    // MARK: - Initializer
    init() {
        self.catalogItemsObserver = CatalogItemsObserver()
        self.catalogItemsSync = CatalogItemSync()
        self.tableViewDataSource = ListTableViewDataSource()
        self.tableViewDelegate = ListTableViewDelegate()
    }
    
    
    // MARK: - ListInteractorInterface
    private(set) var tableViewDataSource: ListTableViewDataSource
    private(set) var tableViewDelegate: ListTableViewDelegate
    
    
    func registerForDataUpdateNotification(_ completion: @escaping (_ shouldUpdateTableView: Bool) -> Void) {
        self.catalogItemsObserver.registerForDataUpdateNotifications() { [weak self] error in
            if error == nil {
                guard let catalogItems = self?.catalogItemsObserver.catalogItems else { return }
                self?.tableViewDataSource.populateCatalogItems(Array(catalogItems))
            } else {
                self?.tableViewDataSource.populateCatalogItems([])
            }
            
            completion(true)
        }
    }
    
    
    func registerForWillDisplayLastCellCallback(_ callback: @escaping () -> Void) {
        self.tableViewDelegate.willDisplayLastCell = callback
    }
    
    
    func fetchItems() -> Promise<Void> {
        return self.catalogItemsSync.synchornize(sinceId: nil, maxId: nil)
    }
    
    
    func fetchNewerItems() -> Promise<Void> {
        guard let newestItemId = self.tableViewDataSource.catalogItems.first?.id else {
            let errorParser = ErrorParser()
            return Promise(error: errorParser.getErrorWithLocalizedDescription("Unable to access oldest item ID"))
        }
        
        return self.catalogItemsSync.synchornize(sinceId: newestItemId, maxId: nil)
    }

    
    func fetchOlderItems() -> Promise<Void> {
        guard let oldestItemId = self.tableViewDataSource.catalogItems.last?.id else {
            let errorParser = ErrorParser()
            return Promise(error: errorParser.getErrorWithLocalizedDescription("Unable to access oldest item ID"))
        }
        
        return self.catalogItemsSync.synchornize(sinceId: nil, maxId: oldestItemId)
    }
    
    
    func clearRedundantCache() {
        // I know it's a pretty ugly hack but I don't have enough time to make it properly.
        guard let realm = try? Realm() else {
            return
        }
        
        let catalogItems = Array(realm.objects(CatalogItem.self))
        
        for index in 0...catalogItems.count - 1 {
            if index < 10 {
                continue
            } else {
                let item = catalogItems[index]
                item.delete(realm: realm)
            }
        }
    }
}
